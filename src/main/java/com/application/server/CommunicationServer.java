package com.application.server;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

public class CommunicationServer extends Thread{
	int portnumber;
	String sessionId;
	DatagramSocket datagramSocket;
	ArrayList<ClientModel> clients;
	DatagramSocket socket;
	public boolean running=true;
	
	public CommunicationServer(int portnumber,ArrayList<ClientModel> clients,String sessionId) {
		this.portnumber = portnumber;
		this.sessionId = sessionId;
		this.clients = clients;
		try {
			datagramSocket = new DatagramSocket(portnumber);
			socket = new DatagramSocket();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		super.run();
		try {
			System.out.println("thread started");
			while(true) {
				byte[] incoming = new byte[6400];
				DatagramPacket datagramPacket = new DatagramPacket(incoming, incoming.length);
				System.out.println("listen here");
				datagramSocket.receive(datagramPacket);
				System.out.println("here is the data");
				InetAddress net = datagramPacket.getAddress();
				final byte[] data = datagramPacket.getData();
				System.out.println(clients.size());
				for(int i=0;i<clients.size();i++) {
					ClientModel model = clients.get(i);
					if(!net.equals(model.getAddress())) {
						DatagramPacket packet = new DatagramPacket(data, data.length,model.getAddress(),50000);;
						socket.send(packet);
						System.out.println("message send");
					}
					System.out.println("hit inside loop");
					}
				System.out.println("hit here");
				System.out.println(running);
				}
		}catch(Exception ex) {
			System.out.println("exception");
			System.out.println(ex.getMessage());
		}}
	
}
