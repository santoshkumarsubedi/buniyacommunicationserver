package com.application.server;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.emitter.Emitter.Listener;
import okhttp3.OkHttpClient;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class Sockets {
	 Socket socket;
	 ArrayList<ClientModel> clients;
	 HashMap<String, CommunicationServer> liveCall;
	 String address;
	 ArrayList<Integer> ports;
	 int[] allPorts= {9000,9004,9005,9006,9007,9008,9009,9010,9011,9012,9013,9014,9015,9016,9017,9018,9019};
	 int[] inUsePorts= {};
	 
	    public Sockets(){
	    	liveCall = new HashMap<String, CommunicationServer>();
	    	ports = new ArrayList<Integer>();
	    	for(int a:allPorts) {
	    		ports.add(a);
	    	}
	    	try {
				address = test();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    	System.out.println(address);
	  
	        try {
	        	 SSLContext sslContext;
	        	  KeyStore keyStore = KeyStore.getInstance("PKCS12");
	              InputStream inputStream = new FileInputStream(new File("/home/accidental-genius/eclipse-workspace/Server/src/main/java/com/application/server/keystore.p12"));
	              keyStore.load(inputStream,"buniya".toCharArray());

	              TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
	              trustManagerFactory.init(keyStore);
	              sslContext = SSLContext.getInstance("TLS");
	              sslContext.init(null,trustManagerFactory.getTrustManagers(),null);
	              HostnameVerifier myHostnameVerifier = new HostnameVerifier() {
	                  @Override
	                  public boolean verify(String hostname, SSLSession session) {
	                      return true;
	                  }
	              };
	              
	              X509TrustManager x509Tm = null;
	              for (TrustManager tm : trustManagerFactory.getTrustManagers()) {
	                  if (tm instanceof X509TrustManager) {
	                      x509Tm = (X509TrustManager) tm;
	                      break;
	                  }
	              }
	              
	            OkHttpClient okHttpClient = new OkHttpClient.Builder().hostnameVerifier(myHostnameVerifier).
	            		sslSocketFactory(sslContext.getSocketFactory(),x509Tm).build();
	        	
	        	IO.Options opts = new IO.Options();
	        	opts.reconnection = true;
	        	opts.reconnectionAttempts=100;
	        	opts.reconnectionDelay=5000;
	        	//opts.callFactory=okHttpClient;
	        	//opts.webSocketFactory = okHttpClient;
	            socket = IO.socket("http://localhost:3000/",opts);
	            
	            socket.connect();
	            socket.emit("join_management","{\"username\":\"CommunicationServer\", \"password\":\"pass123\"}");
	            socket.on("management", new Emitter.Listener() {
	                @Override
	                public void call(Object... args) {
	                    System.out.println(args[0]);
	                }
	                
	            });
	            
	            socket.on("create-server",new Listener() {	
					@Override
					public void call(Object... args) {
						String data=String.valueOf(args[0]);
						String type;
						System.out.println("server create request");
						clients = new ArrayList<ClientModel>();
						try {
							JSONObject jsonObject = new JSONObject(data);
							JSONArray jsonArray = jsonObject.getJSONArray("data");
							type = jsonObject.getString("type");
							String sessionId = jsonObject.getString("SessionId");
							
							for(int i=0;i<jsonArray.length();i++) {
								JSONObject object = jsonArray.getJSONObject(i);
								ClientModel model = new ClientModel();
								String username = object.getString("user");
								InetAddress inetAddress = InetAddress.getByName((object.getString("address")));
								model.setUsername(username);
								model.setAddress(inetAddress);
								clients.add(model);
							}
							CommunicationServer server=null;
							int portnumber=0;
							try {
								System.out.println("hit 1");
								 portnumber = ports.remove(0);
							server = new CommunicationServer(portnumber, clients,sessionId);
							}catch(Exception ex) {
								System.out.println(ex.getMessage());
								 portnumber = ports.remove(0);
								server = new CommunicationServer(portnumber, clients, sessionId);
							}finally {
								System.out.println("hit 2");
								liveCall.put(sessionId,server);
								server.start();
							}
							
							SimpleDateFormat datetosend = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
							Date date = new Date();
							String currentDate = datetosend.format(date);
							System.out.println(portnumber);
							if(type.equals("personal")) {
								socket.emit("server-info", "{\"PORT\":\""+portnumber+"\",\"IPAddress\":\""+address+"\",\"SessionId\":\""+sessionId+"\",\"date\":\""+currentDate+"\"}");
							}else if(type.equals("group")) {
								socket.emit("server-info-department", "{\"PORT\":\""+portnumber+"\",\"IPAddress\":\""+address+"\",\"SessionId\":\""+sessionId+"\",\"date\":\""+currentDate+"\"}");
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (UnknownHostException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
				});
	            
	            socket.on("call-ended",new Listener() {	
					@Override
					public void call(Object... args) {
						String data= args[0].toString();
						try {
							JSONObject obj = new JSONObject(data);
							String sessionId = obj.getString("SessionId");
							CommunicationServer server = liveCall.get(sessionId);
							if(server!=null) {
								server.running = false;
							}
						}catch(JSONException ex) {
							
						}
					}
				});
	            
	            socket.on("add-user",new Listener() {
					@Override
					public void call(Object... args) {
						String data = args[0].toString();
						try {
						JSONObject obj = new JSONObject(data);
						String sessionId = obj.getString("SessionId");
						CommunicationServer server = liveCall.get(sessionId);
						if(server!=null) {
							ClientModel model = new ClientModel();
							model.setUsername(obj.getString("username"));
							model.setAddress(InetAddress.getByName(obj.getString("address")));
							server.clients.add(model);
							liveCall.replace(sessionId, server);
						}
						}catch(JSONException ex) {
							ex.printStackTrace();
						} catch (UnknownHostException e) {
							e.printStackTrace();
						}
						
					}
				});
	            
	            socket.on("remove-user", new Listener() {
					
					@Override
					public void call(Object... args) {
						String data = args[0].toString();
						try {
							JSONObject obj = new JSONObject(data);
							String sessionId = obj.getString("SessionId");
							String username = obj.getString("username");
							CommunicationServer server = liveCall.get(sessionId);
							if(server!=null) {
								for(ClientModel model:server.clients) {
									if(username.equals(model.getUsername())) {
										server.clients.remove(model);
										liveCall.replace(sessionId, server);
										break;
									}
								}
							}
						}catch(JSONException ex) {
							ex.printStackTrace();	}
					}
				});
	            
	            socket.on("call-started",new Listener() {	
					@Override
					public void call(Object... args) {
						String data= args[0].toString();
						try {
							JSONObject obj = new JSONObject(data);
							String sessionId = obj.getString("SessionId");
							CommunicationServer server = liveCall.get(sessionId);
							if(server!=null) {
								server.start();
							}
						}catch(JSONException ex) {
							
						}
						
					}
				});
	            
	            socket.on(Socket.EVENT_DISCONNECT,new Listener() {
					
					@Override
					public void call(Object... args) {
						System.out.println(args[0]);
						
					}
				});
  socket.on(Socket.EVENT_RECONNECT,new Listener() {
					
					@Override
					public void call(Object... args) {
						socket.emit("join_management","{\"username\":\"CommunicationServer\", \"password\":\"pass123\"}");
						System.out.println("reconnect"+args[0]);
						
					}
				});
	            
	        } catch (URISyntaxException ex) {
	            Logger.getLogger(Sockets.class.getName()).log(Level.SEVERE, null, ex);
	            System.out.println(ex.getMessage());
	        }catch(CertificateException ex) {
	        	
	        } catch (KeyStoreException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchAlgorithmException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (KeyManagementException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    }
	    
	    public String test() throws Exception {
	    	String address = null;
	        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();

	        while (interfaces.hasMoreElements()) {
	            NetworkInterface networkInterface = interfaces.nextElement();
	            // drop inactive
	            if (!networkInterface.isUp())
	                continue;

	            // smth we can explore
	            Enumeration<InetAddress> addresses = networkInterface.getInetAddresses();
	            while(addresses.hasMoreElements()) {
	                InetAddress addr = addresses.nextElement();
	                if(networkInterface.getDisplayName().equals("wlp3s0")) {
	                	address = addr.getHostAddress();
	                	
	                }
	            }
	        }
	        return address;
	    }
	    
	    public void createServer() {
	    	
	    }
}
