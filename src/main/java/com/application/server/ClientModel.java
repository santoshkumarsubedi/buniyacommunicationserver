package com.application.server;

import java.net.InetAddress;

public class ClientModel {
	private String username;
	private InetAddress address;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public InetAddress getAddress() {
		return address;
	}
	public void setAddress(InetAddress address) {
		this.address = address;
	}
	
	

}
